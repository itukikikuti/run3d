#include "XLibrary11.hpp"
using namespace XLibrary;

enum Mode
{
    Title,
    Game,
};

int MAIN()
{
    Mode mode = Title;

    Camera camera;
    camera.SetupPerspective(60.0f, 0.1f, 1000.0f);
    camera.position = Float3(0.0f, 3.0f, -3.0f);
    camera.angles.x = 30.0f;
    camera.color = Float4(0.0f, 0.75f, 1.0f, 1.0f);

    Texture playerTexture(L"player.png");
    Texture laneTexture(L"lane.png");
    Texture blockTexture(L"block.png");

    Light light;
    light.type = Light::Type::Directional;
    light.angles = Float3(-50.0f, 30.0f, 0.0f);
    light.Update();

    Mesh player;
    player.CreateSphere();
    player.GetMaterial().SetTexture(0, &playerTexture);
    player.angles.y = 180.0f;

    Mesh lane;
    lane.CreatePlane(Float2(6.0f, 2000.0f));
    lane.GetMaterial().SetTexture(0, &laneTexture);
    lane.position.y = -0.5f;
    lane.position.z = 500.0f;
    lane.angles.x = 90.0f;

    Mesh block;
    block.CreateCube();
    block.GetMaterial().SetTexture(0, &blockTexture);

    Text titleText(L"よけろ！鳥！");
    titleText.position.y = 1.5f;
    titleText.scale = 0.05f;
    titleText.color = Float4(1.0f, 0.0f, 0.0f, 1.0f);

    Text speedText(L"0m/s");
    speedText.position.y = 2.5f;
    speedText.scale = 0.05f;

    float playerPosition = 0.0f;
    float speed = 0.1f;

    float interval = 0.0f;

    std::vector<Float3> blockPositions;

    while (Refresh())
    {
        switch (mode)
        {
        case Title:
            camera.Update();

            if (Input::GetKeyDown(VK_RETURN))
            {
                playerPosition = 0.0f;
                speed = 0.1f;
                interval = 0.0f;
                blockPositions.clear();
                mode = Game;
            }

            titleText.Draw();

            speedText.position.y += (2.5f - speedText.position.y) * 0.1f;
            speedText.scale += (0.05f - speedText.scale) * 0.1f;
            speedText.Draw();

            player.position.x = 0.0f;
            player.angles.x -= speed * 100.0f;
            player.Draw();

            break;

        case Game:
            camera.Update();

            if (Input::GetKeyDown(VK_LEFT))
            {
                playerPosition -= 2.0f;

                if (playerPosition < -2.0f)
                    playerPosition = -2.0f;
            }

            if (Input::GetKeyDown(VK_RIGHT))
            {
                playerPosition += 2.0f;

                if (playerPosition > 2.0f)
                    playerPosition = 2.0f;
            }

            speed += 0.0001f;

            speedText.Create(std::to_wstring(int(speed * Timer::GetFrameRate())) + L"m/s");
            speedText.position.y += (1.0f - speedText.position.y) * 0.1f;
            speedText.scale += (0.02f - speedText.scale) * 0.1f;
            speedText.Draw();

            player.position.x += (playerPosition - player.position.x) * 0.1f;
            player.angles.x -= speed * 100.0f;
            player.Draw();

            lane.Draw();

            interval += speed;

            if (interval > 10.0f)
            {
                interval = 0.0f;
                blockPositions.push_back(Float3(Random::Range(-1, 1) * 2.0f, 0.0f, 100.0f));
            }

            for (int i = 0; i < blockPositions.size(); i++)
            {
                if (blockPositions[i].z < -5.0f)
                {
                    blockPositions.erase(blockPositions.begin() + i);
                    i--;
                    continue;
                }

                blockPositions[i].z -= speed;
                block.position = blockPositions[i];
                block.Draw();

                if (player.position.x > blockPositions[i].x - 1.0f &&
                    player.position.x < blockPositions[i].x + 1.0f &&
                    player.position.z > blockPositions[i].z - 1.0f &&
                    player.position.z < blockPositions[i].z + 1.0f)
                {
                    mode = Title;
                }
            }

            break;
        }
    }

    return 0;
}
